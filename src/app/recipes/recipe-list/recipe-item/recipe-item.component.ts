import { Input } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { Recipe } from '../../recipe.mode';

@Component({
  selector: 'app-recipe-item',
  templateUrl: './recipe-item.component.html',
  styleUrls: ['./recipe-item.component.scss']
})
export class RecipeItemComponent implements OnInit {
  @Input() recipe: Recipe | undefined;
  constructor() { }

  ngOnInit(): void {
  }

}
